package example.com.myapp;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class CustomActivity extends ActionBarActivity {

    private static final String TAG = CustomActivity.class.getName();
    private static final String API_PUBLIC_TOKEN = "{YOUR_API_PUBLIC_TOKEN}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        // Check if activity was started via deeplink
        Uri targetUrl = getIntent().getData();
        if (targetUrl != null) {
            String claimPath = targetUrl.getQueryParameter("claimPath");
            // You can also check the query string easily.
            String myParam = targetUrl.getQueryParameter("myparam");
            // check if the user has claimed your reward
            String claimNotificationUrl = "http://api.juliusapp.com" + claimPath + "?apt=" + API_PUBLIC_TOKEN;
            new GetClaimNotificationTask().execute(claimNotificationUrl);
        }
    }

    // Uses AsyncTask to create a task away from the main UI thread.
    private class GetClaimNotificationTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... urls) {
            getClaimNotification(urls[0]);
            return null;
        }
    }

    /**
     * method for checking if claim was made on your reward
     *
     * @param claimNotificationUrl
     */
    private void getClaimNotification(String claimNotificationUrl) {
        URL obj = null;
        HttpURLConnection urlConnection = null;
        try {
            obj = new URL(claimNotificationUrl);
            urlConnection = (HttpURLConnection) obj.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String response = readStream(in);
            Log.d(TAG, "response=" + response);
            // Here is where you would put code to do deal with the case that a reward has been successfully claimed,
            // e.g. enable the reward content for the user
        } catch (Exception e) {
            Log.e(TAG, "error validating user's claim: " + e.getMessage());
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }
}
